package jp.alhinc.uchiba_aya.calculate_sales;


// 使用するクラスをインポート

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) throws IOException{

// 支店番号と支店名をセットにしてみる
		Map<String, String> map = new HashMap<String, String>();
		Map<String, Integer> map1 = new HashMap<String, Integer>();

// 支店定義ファイルを開く
		//System.out.println("ここにあるファイルを開きます=>" + args[0]);
		Path filePath = Paths.get(args[0], "branch.lst");

		if(!Files.exists(filePath)){  // 支店定義ファイルがない場合
			System.out.println("支店定義ファイルが存在しません");
			return;
		}
		BufferedReader br = null;
		try{
			File file = new File(args[0], "branch.lst");  // 引数1にファイルがある場所、引数2にファイル名を指定する
			FileReader fr = new FileReader(file);  // branch.lstを読み込むためのクラス
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null){  // 文字が書いてある行を1行ずつlineって変数名に置き換えている
				//System.out.println(line);  // 001,札幌支店 ～～

				String[] shiten = line.split(",");  // ,で文字列を区別して配列に入れてみる
				map.put(shiten[0], shiten[1]);  // mapに格納してみる
				map1.put(shiten[0], 0);  // volueを0にすることが可能！

				if(!shiten[0].matches("^[0-9]{3}")){  // 支店番号が3桁じゃなければ
					System.out.println("支店定義ファイルのフォーマットが不正です");
				return;
				}
			}
		}catch(IOException e){
			System.out.println("エラーが発生しました");  // ファイルが開けなかったらここが表示
		}finally{
			if(br != null){
				try{
					br.close();
				}catch(IOException e){
					System.out.println("closeできませんでした");
				}
			}
		}

// 売上ファイルを読み込む
		BufferedReader reader = null;
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File dir, String filename) {
				// 拡張子を指定する
				File file = new File(dir, filename);
				return file.isFile() && filename.matches("\\d{8}\\.rcd");
			}
		};

		// 売上ファイルが連番になっていない場合
		File[] files = new File(args[0]).listFiles(filter);
		Arrays.sort(files);

		int firstFile = Integer.parseInt(files[0].getName().substring(0, 8));
		//System.out.println(files[0]);
		int lastFile = Integer.parseInt(files[files.length - 1].getName().substring(0, 8));
		int sum = firstFile + files.length - lastFile;
		if (sum != 1) {
			System.out.println("売上ファイル名が連番になっていません");
			return;
		}
		try{
			// 売上ファイルの数だけ繰り返し
			for(int i = 0; i < files.length; i++){  // forは繰り返し！！
			// 拡張子.rcd、数字8桁以外のファイルを検索する
				if(!files[i].getName().matches("^[0-9]{8}.rcd$")) {
					//System.out.println(files[i]);  // こっちは不要  //8桁のファイル名でないもの
					continue;
				}
				//System.out.println(files[i]);  // こっちが必要
			// 拡張子.rcd、数字8桁のファイルの中身を1行ずつ2行目まで読み込む
				File file = new File(files[i],"");
				FileReader fr = new FileReader(file);
				reader = new BufferedReader(fr);

				String branchNum = reader.readLine();  // 売上ファイルの1行目
				//System.out.println(shiten1);
				int sale2 = Integer.parseInt(reader.readLine());  // 売上ファイルの2行目 String→int型に変更

				//System.out.println(sale2);
				String line3 = reader.readLine();  // 売上ファイルの3行目以降
				//System.out.println(line3);

				if(line3 != null){  // 3行以上があれば終了
					System.out.println(files[i].getName() + "のフォーマットが不正です");
					System.exit(0);
				}

				if(map.containsKey(branchNum)){
					int total = map1.get(branchNum) + sale2;  // map1.get(shiten1)は初期値0からスタート！！

					if(total >= 1000000000){  // 合計金額が10桁超えたとき[0-9]{10,}
						System.out.println("合計金額が10桁を超えました");
						System.exit(0);
					}
			// 売上ファイルを計算したのをmapにぶちこむ。読み込むたびに値が足されていく
					map1.put(branchNum, total);
				}else{  // 支店に該当がない場合
					System.out.println(files[i].getName() + "の支店コードが不正です");
					System.exit(0);
				}
			}
			// 売上ファイルを閉じる
			reader.close();
		}catch(IOException e){
			System.out.println(e);
		}

// 支店別集計ファイルを作成する
		File file = new File(args[0], "branch.out");
		FileWriter fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);

		try{
			//System.out.println(map);  //{001=札幌支店, 002=仙台支店, 003=東京支店, 004=名古屋支店, 005=大阪支店}
			//System.out.println(map1);  //{001=125195, 002=458210, 003=68500, 004=465850, 005=156320}
			//int total = map1.values();
			for(Map.Entry<String, String> NoName : map.entrySet()){//for(型 変数名 : 式){
				//System.out.println("map,map1のkey" + "," + "mapの値" + "," + "map1の値");

				System.out.println(NoName.getKey() + "," + NoName.getValue() + "," + map1.get(NoName.getKey()));
				bw.write(NoName.getKey() + "," + NoName.getValue() + "," + map1.get(NoName.getKey()));
				bw.newLine();
			}
			bw.close();
		}catch(IOException e
				){
			System.out.println(e);
		}
	}
}